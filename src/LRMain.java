

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jgap.Configuration;
import org.jgap.InvalidConfigurationException;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.impl.GPGenotype;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.RDFReader;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Selector;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import de.uni_leipzig.simba.cache.Cache;
import de.uni_leipzig.simba.cache.MemoryCache;
import de.uni_leipzig.simba.data.Mapping;
import de.uni_leipzig.simba.genetics.core.ExpressionProblem;
import de.uni_leipzig.simba.genetics.core.LinkSpecGeneticLearnerConfig;
import de.uni_leipzig.simba.genetics.core.Metric;
import de.uni_leipzig.simba.genetics.core.PseudoFMeasureFitnessFunction;
import de.uni_leipzig.simba.genetics.evaluation.pseudomeasures.EvaluationPseudoMemory;
import de.uni_leipzig.simba.genetics.learner.UnSupervisedLearnerParameters;
import de.uni_leipzig.simba.genetics.learner.UnsupervisedLearner;
import de.uni_leipzig.simba.genetics.util.PropertyMapping;
import de.uni_leipzig.simba.io.ConfigReader;
import de.uni_leipzig.simba.io.KBInfo;
import de.uni_leipzig.simba.learning.learner.EuclidMain;
import de.uni_leipzig.simba.util.Clock;

/**
 * Example code on how to run EUCLID upon Ninas files.
 * @author Klaus Lyko
 *
 */
public class LRMain {
	public static Logger logger = Logger.getLogger(LRMain.class);
//	KBInfo sInfo = new KBInfo();
//	KBInfo tInfo = new KBInfo();
	public static Logger GPlogger = Logger.getLogger(GPGenotype.class);
	/**Separation for the serialization of the Mapping*/
	public static final String SEP = " ";
	
	/**
	 * Basic method to read NT Files out of URL to local file and building Cache for it.
	 * @param address URL of the NT file
	 * @param dest File for the local copy
	 * @return Parsed cache.
	 * @throws IOException
	 */
	public static File NTFileFromURL(String address, String dest) throws IOException {
		URL url = new URL(address);
		File f = new File(dest);
		FileUtils.copyURLToFile(url, f);
		return f;
	}
	
	/**
	 * Parses N-Triples file to a cache representation. Basically reads the N-Triples with Jena,
	 * iterates over all statements and adds them to a Cache.
	 * Be aware this is a quick hack, maybe noisy and buggy.
	 * @param file The N-Triples file.
	 * @return Cache representation hopefully holding all triples of the underlying N-Triples.
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static Cache NTFileToCache(File file) throws UnsupportedEncodingException, FileNotFoundException {
		Cache cache = new MemoryCache();
		
		InputStream in = new FileInputStream(file);
		InputStreamReader reader = new InputStreamReader(in, "UTF8");
		
		
		Model model = ModelFactory.createDefaultModel();
		RDFReader r = model.getReader("N-TRIPLE");
		r.read(model, reader, null);
		
		System.out.println("model "+model.size());


		StmtIterator it =  model.listStatements();
		while (it.hasNext()) {
		     Statement stmt = it.next();
		     // do your stuff with the Statement (which is a triple)
//		     System.out.println(stmt.getSubject().toString()+" - "+stmt.getPredicate().toString()+" - "+stmt.getObject().toString());
		     String s = stmt.getSubject().toString();
		     String p = stmt.getPredicate().toString();
		     String o = stmt.getObject().toString();
//		     if(p.s)
		     cache.addTriple(s,p,o);
		}
		
		System.out.println("Cache of size "+cache.size()+" buildt");
		return cache;
	}

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InvalidConfigurationException 
	 */
	public static void main(String[] args) throws IOException, InvalidConfigurationException {
		String dirs[] = {"10K","50K"};
		String subs[] = {"complex","semantics","simple","structure","value"};
		int runs[] = {1,2,3};
		String baseURL = "http://users.ics.forth.gr/~jsaveta/ISWC2015_experiments/";
			for(String dir:dirs) {
				for(String sub:subs) { 
				
					String url1 = baseURL+dir+"/"+sub+"/source.nt"; //"http://users.ics.forth.gr/~jsaveta/ISWC2015_experiments/10K/value/source.nt";
					String url2 = baseURL+dir+"/"+sub+"/target.nt";//"http://users.ics.forth.gr/~jsaveta/ISWC2015_experiments/10K/value/target.nt";
					GPlogger.setLevel(Level.ERROR);
					logger.setLevel(Level.ALL);
					File sourceFile = NTFileFromURL(url1, "source.nt");
					File targetFile = NTFileFromURL(url2, "target.nt");
					// for debug
					LRMain lrm = new LRMain();
					lrm.setOutStreams("euclid_nina_"+dir+"_sub");
					Map<String, Cache> sourceMap = inspectModel(sourceFile);
					Map<String, Cache> targetMap = inspectModel(targetFile);
					for(int run:runs) {
						Mapping globalMap = new Mapping();						
						boolean append = false;
						File mapFileEAGLE = new File("eagle_map_"+dir+"_"+sub+"run"+run+".txt");
						for(String p1 : sourceMap.keySet()) {
							if(targetMap.containsKey(p1)) {
								logger.debug("Running EAGLE on rdf:type "+p1);
								KBInfo sI = buildInfo(sourceFile, sourceMap.get(p1).getAllProperties(), p1, "x");
								KBInfo tI = buildInfo(targetFile, targetMap.get(p1).getAllProperties(), p1, "y");
								
								/*
								 * Property Mappings may improve results.
								 * For baseline purposes i basically say EAGLE it may map all availlable 
								 * properties.
								 */
								boolean mapAllProperties = true;
								PropertyMapping propMap = new PropertyMapping();	
								for(String sProp : sourceMap.get(p1).getAllProperties()) {
									if(mapAllProperties)
									for(String tProp : targetMap.get(p1).getAllProperties()) {
										if(!sProp.endsWith("#type") && !tProp.endsWith("#type")) {// avoid linking with types?
											propMap.addStringPropertyMatch(sProp, tProp);
										}
									}else {//map only corresponding properties
										if(!sProp.endsWith("#type")) // avoid types
											if(targetMap.get(p1).getAllProperties().contains("sProp")) {
												propMap.addStringPropertyMatch(sProp, sProp);
											}
									}
								}
								System.out.println("PropMap.size="+propMap);
								// neccesary for EAGLE setup
								ConfigReader cR = new ConfigReader();
								cR.sourceInfo = sI; cR.targetInfo = tI;
								logger.debug("cache Sizes: "+sourceMap.get(p1).size()+" and "+targetMap.get(p1).size());
								EAGLERunner eR= new EAGLERunner();
								Mapping a_map = eR.runEAGLE( sourceMap.get(p1), targetMap.get(p1), propMap, cR);
								System.out.println("Learned map: "+a_map.size());
								logger.debug("Learned map: "+a_map.size()+" serializing...");
								serializeMapping(a_map,mapFileEAGLE, append);
								globalMap.map.putAll(a_map.map);
								append = true;//add other mappings to serialization
							} //class does exist for target
							else {System.err.println("No target class "+p1+" found!");}
						} // for all classes
					}//runs
				}//subs		
		}//dirs
		
/**  EUCLID class-wise**/		
//		String types[] = {"linear", "disjunctive", "conjunctive"};
//		String typ = "disjunctive"; //disjunctive //conjunctive
//		/**This parameter defines the coverage of the properties shared ba all instances to be regarded by EUCLID*/
//		double coverage = 0.9; // 0.6 only rdf:type
//		int iterations = 3;
//		String outputFile = "results_euclid_"+typ+".txt";

//		boolean append = false;
//		for(String p1 : sourceMap.keySet()) {
//			if(targetMap.containsKey(p1)) {
//				System.out.println("Euclid for rdf:type"+p1+" sC.size="+sourceMap.size()+" - tC.size="+targetMap.get(p1).size());
//				EuclidMain em = new EuclidMain();
//				Mapping mapP1 = em.runEuclid(sourceMap.get(p1), targetMap.get(p1), coverage, typ, iterations);
//				serializeMapping(mapP1, new File(outputFile), append);
//				append = true;
//			} else {
//				System.err.println("Not found source type "+p1+" for target");
//			}
//			
//		}
/** EUCLID complete**/
//		// params
//		String url1 = "http://users.ics.forth.gr/~jsaveta/ISWC2015_experiments/10K/value/source.nt";
//		String url2 = "http://users.ics.forth.gr/~jsaveta/ISWC2015_experiments/10K/value/target.nt";
//		
//		String types[] = {"linear", "disjunctive", "conjunctive"};
//		String type = "linear"; //disjunctive //conjunctive
//		/**This parameter defines the coverage of the properties shared ba all instances to be regarded by EUCLID*/
//		double coverage = 0.4; // 0.6 only rdf:type
//		int iterations = 10;
//
		
		
//
//		try {
//			//1st parse Caches
//			//
//			// This avoids handling them via build-in LIMES SPARQL Query modules.
//			// Thus throwing all data about all instances together, without any restrictions
//			// due to rdf:type and such. This means EUCLID won't be as precise if we would parse Caches
//			// using LIMES build-in XML definition to restrict to certain types
//			Cache sC = NTFileFromURLToCache(url1, "source.nt");
//			Cache tC = NTFileFromURLToCache(url2, "target.nt");
//		
//			// running EUCLID
//			for(String typ : types) {
//			
//				Mapping map = em.runEuclid(sC, tC, coverage, typ, iterations);
//				String outputFile = "results_euclid_"+typ+".txt";
//				
//				// write results in your output format to the specified file
//				serializeMapping(map, new File(outputFile));
//			}
//			
//		
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
			
	
	}
	
	/**
	 * Writes Mapping according to Ninas format to file.
	 * @param map Mapping to serialize
	 * @param mapFile File.
	 * @throws IOException
	 */
	public static void serializeMapping(Mapping map, File mapFile, boolean append) throws IOException {
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(mapFile, append)));
		
		// iterating mapping
		for(String uri1 : map.map.keySet()) {
			for(String uri2 : map.map.get(uri1).keySet()) {
				writer.println(expandURL(uri1)+SEP+expandURL(uri2));
			}
				
		}
		
		writer.flush();
		writer.close();
	}
	
	public static String expandURL(String url) {
		String result = url;
		if(url.startsWith("http://")) {
			result ="<"+url+">";
		}
		return result;
	}
	
	/**
	 * Example dummy method on how to manually fill caches based upon triples.
	 * @return Cache instance
	 */
	public static Cache buildDummyCache() {
		//init a cache
		Cache cache = new MemoryCache();
		String props[] = {"ex:name", "ex:someProperty"};
		Random rand = new Random();
		
		for(int i = 0; i<10; i++) {
			
			for(String prop : props) {
				//add triples manually
				cache.addTriple("ex"+i, prop, "text"+rand.nextInt(100));
			}
		}
		return cache;
	}
	
	public void setOutStreams(String name) {
		try {
			File stdFile = new File(name+"_stdOut.txt");
			PrintStream stdOut;
			stdOut = new PrintStream(new FileOutputStream(stdFile, false));
			File errFile = new File(name+"_errOut.txt");
			PrintStream errOut = new PrintStream(new FileOutputStream(errFile, false));
			System.setErr(errOut);
			System.setOut(stdOut);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Map<String, Cache> inspectModel(File file) throws UnsupportedEncodingException, FileNotFoundException {
		HashMap<String, Cache> map = new HashMap<String, Cache>();
		InputStream in = new FileInputStream(file);
		InputStreamReader reader = new InputStreamReader(in, "UTF8");
		
		
		Model model = ModelFactory.createDefaultModel();
		RDFReader r = model.getReader("N-TRIPLE");
		r.read(model, reader, null);
		
		System.out.println("model "+model.size());

		
		Property pType = model.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
		NodeIterator nodeIt = model.listObjectsOfProperty(pType);
		int iType = 0;
		while(nodeIt.hasNext()) {
			RDFNode node = nodeIt.next();
			
			System.out.println(iType+++". type: "+node.toString());
			ResIterator resIt = model.listSubjectsWithProperty(pType, node);
			Cache cache = new MemoryCache();
			while(resIt.hasNext()) {
				Resource s = resIt.next();

				Selector selector = new SimpleSelector(s, null,(RDFNode) null);
				
				StmtIterator stmtIt = model.listStatements(selector);
				while(stmtIt.hasNext()) {
					Statement stmt = stmtIt.next();
					if(stmt.getPredicate() != pType) {
						 String sh = stmt.getSubject().toString();
					     String p = stmt.getPredicate().toString();
					     String o = stmt.getObject().toString();					     
					     cache.addTriple(sh,p,o);
					}
				}
			}
			map.put(node.toString(), cache);
		}// for each rdf:type
		return map;
	}
	
	public static KBInfo buildInfo(File file, Set<String> properties, String classRestriction, String var) {
		KBInfo info = new KBInfo();
		info.endpoint = file.getAbsolutePath();
		info.type = "N3";
		info.var = var;
		for(String prop : properties) {
			info.properties.add(prop);
			info.functions.put(prop, new HashMap<String, String>());
			info.functions.get(prop).put(prop, "lowercase");
		}
		//?x rdf:type http://www.okkam.org/ontology_person1.owl#Person
		info.restrictions.add("?"+var+" rdf:type "+classRestriction);
		return info;
	}
	

}
