import java.util.List;

import de.uni_leipzig.simba.cache.Cache;
import de.uni_leipzig.simba.data.Mapping;
import de.uni_leipzig.simba.learning.learner.EuclidMain;
import de.uni_leipzig.simba.selfconfig.ComplexClassifier;
import de.uni_leipzig.simba.selfconfig.DisjunctiveMeshSelfConfigurator;
import de.uni_leipzig.simba.selfconfig.LinearMeshSelfConfigurator;
import de.uni_leipzig.simba.selfconfig.MeshBasedSelfConfigurator;
import de.uni_leipzig.simba.selfconfig.PseudoMeasures;
import de.uni_leipzig.simba.selfconfig.SimpleClassifier;

/**
 * Wrap around EuclidMain to catch any errors.
 * @author Klaus Lyko
 *
 */
public class EUCLIDRunner extends EuclidMain{

		public Mapping runEuclid(Cache sC, Cache tC, double coverage, String type, int iterations) {	
			MeshBasedSelfConfigurator lsc;
	        if (type.toLowerCase().startsWith("l")) {
	            lsc = new LinearMeshSelfConfigurator(sC, tC, coverage, 1d);
	        } else if (type.toLowerCase().startsWith("d")) {
	            lsc = new DisjunctiveMeshSelfConfigurator(sC, tC, coverage, 1d);
	        } else {
	            lsc = new MeshBasedSelfConfigurator(sC, tC, coverage, 1d);
	        }
	        lsc.setMeasure(new PseudoMeasures());
	        System.out.println("Running EUCLID "+type+" coverage="+coverage+". Computing simple classifiers...");
	        long begin = System.currentTimeMillis();
	        List<SimpleClassifier> cp = lsc.getBestInitialClassifiers();
	        System.out.println("Simple classifiers....");
	        for(SimpleClassifier sc : cp)
	        	System.out.println(sc);
	        if(cp.size() > 0) {
	        	long middle = System.currentTimeMillis();
	   	        System.out.println(cp.size()+" simple classifiers computed in "+(middle-begin)+" ms. Computing complex classifier for "+iterations+" iterations...");
	   	        ComplexClassifier cc = lsc.getZoomedHillTop(5, iterations, cp);
	   	     	
	   	        long end = System.currentTimeMillis();
	   	        System.out.println("Eculid finished after "+(end-begin)+" ms = "+(end-begin)/1000+" s." );
	   	        System.out.println("Complex Classifier= "+cc);
	   	        System.out.println("Mapping size = "+cc.mapping.size());
	   			return cc.mapping;
	        } else {
	        	System.out.println("No simple classifier found. Returning empty map");
	        	return new Mapping();
	        }
	     
		}
}
